@extends('layouts.master')

@section('content')
<div class="big-title">
    <div class="container">
        <h1 class="entry-title">{{ $project->getTranslation()->title }}</h1>
        <div class="breadcrumb">
            <div class="container">
                <ul class="tm_bread_crumb">
                    <li class="top"><a href="/">Home</a></li>
                    <li class="sub"><a href="/projects">Projects</a></li>
                    <li class="sub tail current">Project Detail</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="main">
    <div class="single-project">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="gallery tm-gallery mb-3">
                        <div class="tm-nav">
                            <span class="tm-next"><i class="fa fa-angle-right"></i></span>
                            <span class="tm-prev"><i class="fa fa-angle-left"></i></span>
                        </div>
                        <div class="project-slider">
                            @foreach ($project->images as $image)
                                <img width="1900" height="1268" src="/images/{{ $image->name }}" alt="{{ $image->alt }}" />
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="single-project-description">
                        <h3 class="heading-title">{{ $project->getTranslation()->title }}</h3>
                        <p>
                            {!! $project->getTranslation()->description !!}
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
