@extends('layouts.master')

@section('content')
<div class="big-title">
    <div class="container">
        <h1 class="entry-title">Projects</h1>
        <div class="breadcrumb">
            <div class="container">
                <ul class="tm_bread_crumb">
                    <li class="top"><a href="/">Home</a></li>
                    <li class="sub tail current">Projects</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="our-projects">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="myportfolio-container minimal-light" id="our-projects">
                    <div class="portfolio-row">
                        <div class="portfolioContainer portfolioContainer-full row" id="da-thumbs">
                            <ul class="project-masonry">
                                @foreach ($projects as $project)
                                <li class="grid-item filter-building our-projects-wrapper col-md-6 col-sm-6 mb-3">
                                    <div class="project-media-cover-wrapper">
                                        <div class="project-entry-media">
                                            @php
                                                $image = $project->images->first();
                                            @endphp

                                            <img src="/images/{{ $image->name ?? '' }}" alt="{{ $image->alt ?? '' }}" />
                                        </div>
                                        <div class="project-entry-cover">
                                            <div class="project-overlay our-projects-container"></div>
                                            <div class="project-content">
                                                <div class="our-projects-categories">
                                                    <a href="/projects/{{ $project->id }}">Project</a>
                                                </div>
                                                <div class="line-clear"></div>
                                                <div class="our-projects-title">
                                                    <a href="/projects/{{ $project->id }}">
                                                        {{ $project->getTranslation()->title }}
                                                    </a>
                                                </div>
                                                <div class="line-clear"></div>
                                                <div class="our-projects-popup">
                                                    <a data-rel="prettyPhoto" href="/images/{{ $image->name ?? '' }}">
                                                        <i class="fa fa-search"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
