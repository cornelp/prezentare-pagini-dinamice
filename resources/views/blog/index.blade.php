@extends('layouts.master')
@section('content')
<div class="big-title">
    <div class="container">
        <h1 class="entry-title">News Mixed</h1>
        <div class="breadcrumb">
            <div class="container">
                <ul class="tm_bread_crumb">
                    <li class="top"><a href="/">Home</a></li>
                    <li class="sub tail current">News Mixed</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="main">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-12">
                @if ($firstPost)
                <div id="first-post" class="post hentry mb-3">
                    <div class="post-thumb">
                        <a href="blog-detail.html">
											<img width="770" height="375" src="/images/blog/{{ $firstPost->image }}" alt="">
										</a>
                    </div>
                    <div class="entry-meta">
                        <span class="post-time">
											<i class="fa fa-clock-o"></i> {{ $firstPost->created_at->format('d.m.Y') }}
										</span>
                        <span class="author">
											<i class="fa fa-user"></i> admin
										</span>
                        <span class="comments-counts">
											{{-- <i class="fa fa-comment"></i> 2 Comments --}}
										</span>
                    </div>
                    <!-- .entry-meta -->
                    <div class="entry-header">
                        <h2 class="entry-title">
                            <a href="/blog/{{ $firstPost->slug }}">{{ $firstPost->title }}</a>
                        </h2>
                    </div>
                    <div class="entry-content">
                    </div>
                    <!-- .entry-content -->
                </div>
                @endif
                <div class="row">
                    <ul class="news-grid">
                        @foreach ($blogs as $blog)
                        <li class="col-md-6 col-sm-12 mb-3">
                            <div class="news-entry-media">
                                <img src="images/blog/{{ $blog->image }}" alt="">
                                <div class="news-entry-cover">
                                    <div class="news-overlay home-blog-container"></div>
                                    <div class="home-blog-popup">
                                        <a data-rel="prettyPhoto" href="images/blog/{{ $blog->image }}">
															<i class="fa fa-search"></i>
														</a>
                                    </div>
                                </div>
                            </div>
                            <div class="home-blog-content">
                                <div class="home-blog-title">
                                    <a href="/blog/{{ $blog->slug }}">{{ $blog->title }}</a>
                                </div>
                                <div class="home-blog-date">{{ $blog->created_at->format('d.m.Y') }}</div>
                                {{-- <div class="home-blog-comment">2</div> --}}
                                <div class="home-blog-desc">
                                    {!! $blog->text !!}
                                </div>
                                <div class="home-blog-readmore">
                                    <a href="/blog/{{ $blog->slug }}">
														Read More
													</a>
                                </div>
                            </div>
                        </li>
                        @endforeach
                    </ul>
                    {{ $blogs->links() }}
                </div>
            </div>
            <div class="col-md-4 col-sm-12">
                <div class="sidebar">
                    <div class="widget">
                        <h3 class="widget-title">Recent News</h3>
                        <div class="recent-posts-list">
                            <div class="recent-posts-thumb">
                                <img width="80" height="53" src="images/blog/thumb/blog_1.jpg" alt="">
                            </div>
                            <div class="recent-posts-info">
                                <a href="blog-detail.html">House Review: Starter Homes</a> July 31, 2015
                            </div>
                        </div>
                        <div class="recent-posts-list">
                            <div class="recent-posts-thumb">
                                <img width="80" height="53" src="images/blog/thumb/blog_2.jpg" alt="">
                            </div>
                            <div class="recent-posts-info">
                                <a href="blog-detail.html">9 Reasons To Go Modular</a> July 31, 2015
                            </div>
                        </div>
                        <div class="recent-posts-list">
                            <div class="recent-posts-thumb">
                                <img width="80" height="53" src="images/blog/thumb/blog_3.jpg" alt="">
                            </div>
                            <div class="recent-posts-info">
                                <a href="blog-detail.html">The Brands Builders Buy Most</a> July 31, 2015
                            </div>
                        </div>
                    </div>
                    <div class="widget">
                        <h3 class="widget-title">Archives</h3>
                        <ul>
                            <li><a href="#">April 2015 - (2)</a></li>
                            <li><a href="#">May 2015 - (1)</a></li>
                            <li><a href="#">June 2015 - (3)</a></li>
                            <li><a href="#">July 2015 - (1)</a></li>
                            <li><a href="#">August 2015 - (2)</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
@endsection
