@extends('layouts.master')

@section('content')
				<div class="big-title">
					<div class="container">
						<h1 class="entry-title">{{ $blog->title }}</h1>
						<div class="breadcrumb">
							<div class="container">
								<ul class="tm_bread_crumb">
									<li class="top"><a href="/">Home</a></li>
									<li class="sub tail current">Blog Detail</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="main">
					<div class="container">
						<div class="row">
							<div class="col-md-8 col-sm-12">
								<div class="single single-post">
									<div class="hentry">
										<div class="post-thumb">
											<img width="270" height="180" src="/images/blog/{{ $blog->image }}" alt="">
										</div>
										<div class="entry-meta row middle">
											<div class="col-sm-8">
												<span class="post-time"><i class="fa fa-clock-o"></i> {{ $blog->created_at->diffForHumans() }}</span>
											</div>
											<div class="col-sm-4">
												<div class="share text-right">
													<span><i class="fa fa-share-alt"></i> Share: </span>
													<span><a target="_blank" href="#"><i class="fa fa-facebook"></i></a></span>
													<span><a target="_blank" href="#"><i class="fa fa-twitter"></i></a></span>
													<span><a target="_blank" href="#"><i class="fa fa-google-plus"></i></a></span>
												</div>
											</div>
										</div>
										<div class="entry-header">
											<h2 class="entry-title">{{ $blog->title }}</h2>
										</div>
										<div class="entry-content">{!! $blog->text !!}</div>
										<!-- .entry-content -->
									</div>
								</div>
							</div>
							<div class="col-md-4 col-sm-12">
								<div class="sidebar">
									<div class="widget">
										<h3 class="widget-title">Recent News</h3>
										<div class="recent-posts-list">
                                            @foreach ($others as $item)
											<div class="recent-posts-thumb">
												<img width="80" height="53" src="/images/blog/{{ $item->image }}" alt="">
											</div>
											<div class="recent-posts-info">
												<a href="/blog/{{ $item->slug }}">{{ $item->title }}</a>
                                                {{ $item->created_at->diffForHumans() }}
											</div>
                                            @endforeach
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
@endsection