{!! Form::open()->get()->route($route) !!}
    {!! Form::text('search', null, request()->search)->placeholder('Cauta') !!}
    {!! Form::text('sort', null, request()->sort)->type('hidden')->attrs(['onChange' => 'submitSearch()']) !!}
    {!! Form::text('order', null, request()->order ?? 'asc')->type('hidden') !!}
{!! Form::close() !!}

@push('js')
    <script>
        let submitSearch = function () {
            $('#search').closest('form').submit();
        };

        $(function () {
            $('#search').on('blur enter', function () {
                submitSearch();
            });
        });
    </script>
@endpush
