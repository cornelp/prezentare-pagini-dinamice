<thead>
    <tr>
        @foreach ($columns as $name => $column)
            <th>
                @if ($name == 'none')
                    {{ $column }}
                @else
                    <a href="#" onclick="sortBy(event, '{{ $name }}')">
                        {{ $column }}
                        @if (request('sort') == $name)
                            <span class="fa fa-sort-{{ request('order') == 'asc' ? 'up' : 'down' }}"></span>
                        @endif
                    </a>
                @endif
            </th>
        @endforeach
    </tr>
</thead>

@push('js')
    <script>
        let sortBy = function (event, name) {
            // get old value for sort
            let oldSort = $('#sort').val();
            let oldOrder = $('#order').val();

            // if old value is the same, change asc to desc
            $('#order').val(oldSort == name && oldOrder == 'asc' ? 'desc' : 'asc');

            $('#sort').val(name).trigger('change');

            event.preventDefault();
        };
    </script>
@endpush
