@extends('layouts.master')

    @section('content')
        @if (count($items))
            @foreach ($items as $item)
                <p>
                    <a href="/instalation-instructions/{{ $item }}">
                        {{ $item }}
                    </a>
                </p>
            @endforeach
        @endif
@endsection
