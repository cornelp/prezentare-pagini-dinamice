@extends('layouts.master')

@section('content')
    <div class="container">
        @if (view()->exists('confident.' . lang()))
            @include('confident.' . lang())
        @endif
    </div>
@endsection
