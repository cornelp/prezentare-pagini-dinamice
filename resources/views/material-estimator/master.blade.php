@extends('layouts.master')

@section('content')
    <div class="container">
        @if (view()->exists('material-estimator.' . lang()))
            @include('material-estimator.' . lang())
        @endif
    </div>
@endsection
