@extends('layouts.master')
@section('content')

<div class="big-title">
    <div class="container">
        <h1 class="entry-title">Contact</h1>
        <div class="breadcrumb">
            <div class="container">
                <ul class="tm_bread_crumb">
                    <li class="top"><a href="/">Home</a></li>
                    <li class="sub tail current">Contact</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="section section-contact">
    <div class="container">
        <div class="row">
            <div class="col-sm-8">
                <div class="mb-3">
                    <h2>Contact us</h2>
                </div>
                <div class="mb-3">
                    <p>

                    </p>
                </div>

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <form class="wpcf7-form" method="POST" action="/contact">
                    @csrf

                    <div class="row">
                        <div class="col-md-4">
                            <div class="wpcf7-form-control-wrap your-name">
                                <input type="text" name="name" value="{{ old('name') }}" class="wpcf7-form-control" placeholder="Your name (required)">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="wpcf7-form-control-wrap your-phone">
                                <input type="tel" name="phone" value="{{ old('phone') }}" class="wpcf7-form-control" placeholder="Your phone">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="wpcf7-form-control-wrap your-email">
                                <input type="email" name="email" value="{{ old('email') }}" class="wpcf7-form-control" placeholder="Your e-mail (required)">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="wpcf7-form-control-wrap your-message">
                                <textarea name="message" cols="40" rows="10" class="wpcf7-form-control" placeholder="Your message (required)">
                                    {{ old('message') }}
                                </textarea>
                            </div>
                            <br>
                            <input type="submit" value="SUBMIT" class="wpcf7-form-control wpcf7-submit">
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-sm-4">
                <div class="mb-2">
                    <div class="section-title">Contact Info</div>
                </div>
                <div class="contact-info">
						<p><i class="fa fa-map-marker"></i> &nbsp; {{ $firstPageInfo->address ?? '' }}</p>
						<p><i class="fa fa-phone"></i> &nbsp; {{ $firstPageInfo->phone ?? '' }}</p>
						<p><i class="fa fa-envelope"></i> &nbsp; <a href="mailto:{{ $firstPageInfo->email ?? '' }}">{{ $firstPageInfo->email ?? '' }}</a></p>
						<p><i class="fa fa-fax"></i> &nbsp; {{ $firstPageInfo->fax ?? '' }}</p>
						<p><i class="fa fa-clock-o"></i> &nbsp; {{ $firstPageInfo->program ?? '' }}</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="section-title mt-3 mb-3">Location</div>
            </div>
            <div class="col-sm-12">
                <iframe src="https://www.google.com/maps/embed?pb=!1m23!1m12!1m3!1d5077.41290726687!2d4.1985776280443625!3d50.48380831791973!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m8!3e6!4m0!4m5!1s0x47c23655f310c917%3A0xa64559954474d4d0!2sRue+de+la+Grattine+51%2C+7100+La+Louvi%C3%A8re%2C+Belgia!3m2!1d50.4838084!2d4.202955!5e0!3m2!1sro!2sro!4v1547817139790" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</div>
@endsection
