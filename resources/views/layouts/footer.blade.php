</div>

<footer class="site-footer">
	<div class="container">
		<div class="row">
			<div class="col-md-4 col-sm-6">
				<div class="widget">
					<a href="./" class="footer-logo">
									<img src="/images/logo.png" alt="Footer Logo"/>
								</a> @lang('frontend.buton_cotatie')
				</div>
				<div class="social">
					<div class="social-menu">
						<ul class="menu">
							<li><a href="http://facebook.com">Facebook</a></li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="widget">
					<h3 class="widget-title"><span>Information</span></h3>
					<ul class="menu">
						<li><a href="/about-us">About Us</a></li>
						<li><a href="/terms">Terms & Condition</a></li>
						<li><a href="/confident">Confidentiality Policy</a></li>
						<li><a href="/cookies">Cookies Policy</a></li>
						<li><a href="/gdpr">GDPR</a></li>
						<li><a href="/contact">Contact</a></li>

				</div>
			</div>
			<div class="col-md-4 col-sm-6">
				<div class="widget">
					<h3 class="widget-title"><span>Office</span></h3>
					<div class="textwidget">
						<p><i class="fa fa-map-marker"></i> &nbsp; {{ $firstPageInfo->address ?? '' }}</p>
						<p><i class="fa fa-phone"></i> &nbsp; {{ $firstPageInfo->phone ?? '' }}</p>
						<p><i class="fa fa-envelope"></i> &nbsp; <a href="mailto:{{ $firstPageInfo->email ?? '' }}">{{ $firstPageInfo->email ?? '' }}</a></p>
						<p><i class="fa fa-fax"></i> &nbsp; {{ $firstPageInfo->fax ?? '' }}</p>
						<p><i class="fa fa-clock-o"></i> &nbsp; {{ $firstPageInfo->program ?? '' }}</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
<div class="copyright">
	<div class="container">
		<div class="row middle">
			<div class="col-md-12 end-md end-lg center">
				© Copyrights 2018 ISO'ART RESINS - Created by <a href="https://xita.ro" target="_blank">XITARO</a>
			</div>
		</div>
	</div>
</div>
</div>
<a class="scrollup"><i class="fa fa-angle-up"></i></a>

<script type='text/javascript' src='/js/jquery.min.js'></script>
<script type='text/javascript' src='/js/jquery-migrate.min.js'></script>
<script type='text/javascript' src='/js/bootstrap.min.js'></script>
<script type='text/javascript' src='/js/jquery.themepunch.tools.min.js'></script>
<script type='text/javascript' src='/js/jquery.themepunch.revolution.min.js'></script>
<script type='text/javascript' src='/js/snap.min.js'></script>
<script type='text/javascript' src='/js/owl.carousel.min.js'></script>
<script type='text/javascript' src='/js/jquery.prettyPhoto.js'></script>
<script type='text/javascript' src='/js/jquery.prettyPhoto.init.min.js'></script>
<script type='text/javascript' src='/js/jQuery.headroom.min.js'></script>
<script type='text/javascript' src='/js/headroom.min.js'></script>
<script type='text/javascript' src='/js/script.js'></script>

<script type='text/javascript' src='/js/extensions/revolution.extension.video.min.js'></script>
<script type='text/javascript' src='/js/extensions/revolution.extension.slideanims.min.js'></script>
<script type='text/javascript' src='/js/extensions/revolution.extension.actions.min.js'></script>
<script type='text/javascript' src='/js/extensions/revolution.extension.layeranimation.min.js'></script>
<script type='text/javascript' src='/js/extensions/revolution.extension.kenburn.min.js'></script>
<script type='text/javascript' src='/js/extensions/revolution.extension.navigation.min.js'></script>
<script type='text/javascript' src='/js/extensions/revolution.extension.migration.min.js'></script>
<script type='text/javascript' src='/js/extensions/revolution.extension.parallax.min.js'></script>

<script>
	$(function () {
				$('button.accept').on('click', function () {
					location.replace('/consent?accept=1');
				});

				// $('button.denie').on('click', function () {
				// 	location.replace('/consent?accept=0');
				// });
			});

</script>

@stack('js')

</body>

</html>