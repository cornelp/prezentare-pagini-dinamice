<div class="section">
    <div class="rev_slider_wrapper fullwidthbanner-container">
        <div id="rev_slider_1" class="rev_slider fullwidthabanner">
            <ul>
                @foreach ($images as $image)
                    <li data-transition="random" data-slotamount="7" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300" data-rotate="0"  data-saveperformance="off"  data-title="Slide">
                        <!-- MAIN IMAGE -->
                        <img src="/images/first-page/{{ $image }}"  alt=""
                        data-bgposition="center top"
                        data-kenburns="on"
                        data-duration="9000"
                        data-ease="Linear.easeNone"
                        data-scalestart="100"
                        data-scaleend="110"
                        data-rotatestart="0"
                        data-rotateend="0"
                        data-offsetstart="0 0"
                        data-offsetend="0 0"
                        class="rev-slidebg" />
                        <!-- LAYERS -->

                        <div class="tp-caption tp-resizeme"
                        data-x="['left','center','center','center']"
                        data-hoffset="['0','0','0','0']"
                        data-y="['top','middle','middle','middle']"
                        data-voffset="['135','-130','-130','-128']"
                        data-width="none"
                        data-height="none"
                        data-whitespace="nowrap"
                        data-visibility="['on','on','on','off']"
                        data-transform_idle="o:1;"
                        data-transform_in="x:right;s:250;e:Power3.easeInOut;"
                        data-transform_out="x:left;s:300;s:300;"
                        data-start="800"
                        data-responsive_offset="on"><img src="/images/banner/line_01.png" alt="" />
                    </div>

                    <div class="tp-caption tp-resizeme"
                    data-x="['left','center','center','center']"
                    data-hoffset="['-1','0','0','0']"
                    data-y="['top','middle','middle','middle']"
                    data-voffset="['419','150','150','128']"
                    data-width="none"
                    data-height="none"
                    data-whitespace="nowrap"
                    data-visibility="['on','on','on','off']"
                    data-transform_idle="o:1;"
                    data-transform_in="x:left;s:250;e:Power3.easeInOut;"
                    data-transform_out="x:right;s:300;s:300;"
                    data-start="1000"
                    data-responsive_offset="on"><img src="/images/banner/line_01.png" alt="" />
                </div>

                <div class="tp-caption tp-resizeme"
                data-x="['left','center','center','left']"
                data-hoffset="['375','-250','-250','135']"
                data-y="['top','middle','middle','top']"
                data-voffset="['0','0','0','0']"
                data-width="none"
                data-height="none"
                data-whitespace="nowrap"
                data-visibility="['on','on','on','off']"
                data-transform_idle="o:1;"
                data-transform_in="y:bottom;s:250;e:Power3.easeInOut;"
                data-transform_out="y:top;s:300;s:300;"
                data-start="1200"
                data-responsive_offset="on"><img src="/images/banner/line_02.png" alt="" />
            </div>

            <div class="tp-caption tp-resizeme"
            data-x="['left','center','center','left']"
            data-hoffset="['890','250','250','635']"
            data-y="['top','middle','middle','top']"
            data-voffset="['0','0','0','0']"
            data-width="none"
            data-height="none"
            data-whitespace="nowrap"
            data-visibility="['on','on','on','off']"
            data-transform_idle="o:1;"
            data-transform_in="y:top;s:250;e:Power3.easeInOut;"
            data-transform_out="y:bottom;s:300;s:300;"
            data-start="1400"
            data-responsive_offset="on"><img src="/images/banner/line_02.png" alt="" />
        </div>

        <div class="tp-caption tp-shape tp-shapewrapper tp-resizeme"
        data-x="['left','center','center','center']"
        data-hoffset="['375','0','0','0']"
        data-y="['top','middle','middle','middle']"
        data-voffset="['135','0','0','0']"
        data-width="['515','500','500','500']"
        data-height="['285','280','280','280']"
        data-whitespace="['normal','nowrap','nowrap','nowrap']"
        data-transform_idle="o:1;"
        data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.9;sY:0.9;skX:0;skY:0;opacity:0;s:400;e:Power3.easeInOut;"
        data-transform_out="opacity:0;s:300;s:300;"
        data-start="1600"
        data-responsive_offset="on"></div>

        <div class="tp-caption slide-text-1 tp-resizeme"
        data-x="['left','center','center','center']"
        data-hoffset="['410','0','0','0']"
        data-y="['top','middle','middle','middle']"
        data-voffset="['165','-110','-110','-110']"
        data-width="none"
        data-height="none"
        data-whitespace="nowrap"
        data-transform_idle="o:1;"
        data-transform_in="opacity:0;s:200;e:Power3.easeInOut;"
        data-transform_out="opacity:0;s:300;s:300;"
        data-start="2000"
        data-splitin="none"
        data-splitout="none"
        data-responsive_offset="on">WE BRING YOUR VISION TO LIFE
    </div>

    <div class="tp-caption tp-resizeme"
    data-x="['left','center','center','center']"
    data-hoffset="['410','0','0','0']"
    data-y="['top','middle','middle','middle']"
    data-voffset="['203','-70','-70','-70']"
    data-width="none"
    data-height="none"
    data-whitespace="nowrap"
    data-transform_idle="o:1;"
    data-transform_in="opacity:0;s:200;e:Power3.easeInOut;"
    data-transform_out="opacity:0;s:300;s:300;"
    data-start="2000"
    data-responsive_offset="on"><img src="/images/banner/line_03.png" alt="" />
    </div>

    <div class="tp-caption slide-text-2 tp-resizeme"
    data-x="['left','center','center','center']"
    data-hoffset="['410','0','0','0']"
    data-y="['top','middle','middle','middle']"
    data-voffset="['216','-40','-40','-40']"
    data-width="none"
    data-height="none"
    data-whitespace="nowrap"
    data-transform_idle="o:1;"
    data-transform_in="opacity:0;s:200;e:Power3.easeInOut;"
    data-transform_out="opacity:0;s:300;s:300;"
    data-start="2000"
    data-splitin="none"
    data-splitout="none"
    data-responsive_offset="on">Kich Out Other Trashy Sites
    </div>

    <div class="tp-caption slide-text-2 tp-resizeme"
    data-x="['left','center','center','center']"
    data-hoffset="['410','0','0','0']"
    data-y="['top','middle','middle','middle']"
    data-voffset="['260','10','10','10']"
    data-width="none"
    data-height="none"
    data-whitespace="nowrap"
    data-transform_idle="o:1;"
    data-transform_in="opacity:0;s:200;e:Power3.easeInOut;"
    data-transform_out="opacity:0;s:300;s:300;"
    data-start="1990"
    data-splitin="none"
    data-splitout="none"
    data-responsive_offset="on">Builder Is Better
    </div>

    <a   class="tp-caption rev-btn tp-resizeme rev-btn-1"
    href="#" target="_self"
    data-x="['left','center','center','center']"
    data-hoffset="['410','-80','-80','-80']"
    data-y="['top','middle','middle','middle']"
    data-voffset="['338','80','80','80']"
    data-width="none"
    data-height="none"
    data-whitespace="nowrap"
    data-transform_idle="o:1;"
    data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power3.easeInOut;"
    data-style_hover="c:rgba(0, 45, 104, 1.00);bg:rgba(255, 255, 255, 1.00);"
    data-transform_in="opacity:0;s:200;e:Power2.easeInOut;"
    data-transform_out="opacity:0;s:300;s:300;"
    data-start="2000"
    data-splitin="none"
    data-splitout="none"
    data-actions=''
    data-responsive_offset="on">MORE </a>

    </li>
@endforeach
</ul>
<div class="tp-bannertimer"></div>
</div>
</div>
</div>
