<!DOCTYPE html>
<html lang="en-US">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0" />
	<link rel="shortcut icon" href="images/favicon.ico" />
	<title> Iso'Art Resins</title>

	<link rel='stylesheet' href='/css/bootstrap.min.css' type='text/css' media='all' />
	<link rel='stylesheet' href='/css/settings.css' type='text/css' media='all' />
	<link rel='stylesheet' href='/css/font-awesome.min.css' type='text/css' media='all' />
	<link rel='stylesheet' href='/css/prettyPhoto.css' type='text/css' media='all' />
	<link rel='stylesheet' href='/css/owl.carousel.css' type='text/css' media='all' />
	<link rel='stylesheet' href='/css/style.css' type='text/css' media='all' />
	<link rel='stylesheet' href='/css/custom.css' type='text/css' media='all' />
	<link rel='stylesheet' href='/css/preloader.css' type='text/css' media='all' />
	<link rel='stylesheet' href='/css/consent.css' type='text/css' media='all' />

	<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
	 rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Droid+Serif:400,700' rel='stylesheet' type='text/css'>
	<link href='//fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900' rel='stylesheet' type='text/css'> @stack('css')
</head>

<body class="header01">
	<div id="loading">
		<div id="loading-center">
			<div id="loading-center-absolute">
				<div class="loader">Loading...</div>
			</div>
		</div>
	</div>
	<div class="snap-drawers">
		<div class="snap-drawer snap-drawer-left">
			<div class="mobile-menu">
				<ul class="menu">
					<li><a href="/">Home</a></li>
					<li>
						<a href="/projects">Projects</a>
						<ul class="sub-menu">
							@foreach ($projects as $project)
							<li><a href="/projects/{{ $project->id }}">{{ $project->getTranslation()->title }}</a></li>
							@endforeach
						</ul>
					</li>
					<li><a href="/instalation-instructions">Installation Instruction</a></li>
					<li>
						<a href="/projects">Color Center</a>
						<ul class="sub-menu">
							@foreach ($projects as $project)
							<li><a href="/projects/{{ $project->id }}">{{ $project->title }}</a></li>
							@endforeach
						</ul>
					</li>
					<li><a href="/material-estimator">Material Estimator</a></li>
					<li><a href="/about-us">About Us</a></li>
					<li><a href="/blog">Blog</a></li>
					<li><a href="/contact">Contact</a></li>
				</ul>
			</div>
		</div>
		<div class="snap-drawer snap-drawer-right"></div>
	</div>
	<div id="page" class="site">
		<div class="top-area site-top">
			<div class="container">
				<div class="row middle">
					<div class="col-md-6 col-xs-6">
						Schimbat
					</div>
					<div class="col-md-6 col-xs-6 text-right">
						<div class="top-menu">
							<form action="/lang" method="POST">
								@csrf @method('PUT')
								<select class="selectpicker" name="lang" data-width="fit" style="padding:5px!important;" onchange="this.form.submit()">
                                        @foreach ($langs as $lang)
                                            <option {{ lang() === $lang->id ? 'selected' : '' }} value="{{ $lang->id }}">{{ $lang->name }}</option>
                                        @endforeach
                                    </select>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<header class="site-header">
			<nav id="site-navigation" class="main-navigation">
				<div class="main-menu">
					<div class="container">
						<div class="row middle">
							<div class="col-md-2 col-xs-8 site-branding">
								<a href="/">
										<img src="/images/logo.png" alt="Logo"/>
									</a>
							</div>
							<div class="col-xs-2 hidden-md hidden-lg">
								<i class="fa fa-search search-btn"></i>
							</div>
							<div class="col-xs-2 hidden-md hidden-lg end">
								<i id="open-left" class="fa fa-navicon"></i>
							</div>
							<div class="col-md-9 hidden-xs hidden-sm nav-left">
								<div class="primary-menu">
									<ul class="menu">
										<li class="menu-item-has-children">
											<a href="/projects">Projects</a>
											<ul class="sub-menu">
												@foreach ($projects as $project)
												<li><a href="/projects/{{ $project->id }}">{{ $project->getTranslation()->title }}</a></li>
												@endforeach
											</ul>
										</li>
										<li class="menu-item-has-children">
											<a href="/products">Products</a>
											<ul class="sub-menu">
												@foreach ($products as $product)
												<li><a href="/products/{{ $product->id }}">{{ $product->getTranslation()->title }}</a></li>
												@endforeach
											</ul>
										</li>
										<li><a href="/instalation-instructions">Installation Instruction</a></li>
										<li>
											<a href="/color-center">Color Center</a>
										</li>
										<li><a href="/material-estimator">Material Estimator</a></li>
										<li><a href="/about-us">About Us</a></li>
										<li><a href="/blog">Blog</a></li>
										<li><a href="/contact">Contact</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</nav>
			<!-- #site-navigation -->
		</header>
		<section id="consent">
			<div class="container">
				<div class="row">
					@if (! session('consent'))
					<div class="consentCard gardnerStyle col-lg-3 col-md-6 col-sm-12 consent-left" id="ConsentCard">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3>Cookies Consent</h3>
							</div>
							<div class="panel-body">
								<p>We use cookies to ensure you get the best experience on our website.</p>
								<ul>
									<li><i class="fa fa-angle-double-right"></i> Read <a href="#">Terms & Conditions</a></li>
									<li><i class="fa fa-angle-double-right"></i> Read <a href="#">Our Cookie Policy</a></li>
									<li><i class="fa fa-angle-double-right"></i> Read <a href="#">Our GDPR Policy</a></li>
								</ul>
								<div class="col-md-12 consentNav text-center">
									<div class="btn-group" role="group">
										{{-- <button type="button" class="btn btn-default denie">I Disagree</button> --}}
										<button type="button" class="btn btn-default accept">I Agree</button>
									</div>
								</div>
							</div>
						</div>
					</div>
					@endif
				</div>
			</div>
		</section>
		<div class="site-content">