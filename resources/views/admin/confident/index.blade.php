@extends('adminlte::page')

@section('content')
    @box
            <ul class="nav nav-tabs">
                @foreach ($langs as $lang)
                    <li class="{{ $loop->first ? 'active' : '' }}"><a data-toggle="tab" href="#{{ $lang->id }}">{{ $lang->name }}</a></li>
                @endforeach
            </ul>

            <div class="tab-content">
                @foreach ($langs as $lang)
                    <div id="{{ $lang->id }}" class="tab-pane fade {{ $loop->first ? 'in active' : '' }}">
                        @include('admin.confident.edit')
                    </div>
                @endforeach
            </div>
    @endbox
@endsection

@include('layouts.summernote')
