@extends('adminlte::page')

@section('content')
    {!! Form::open()->post()->route('admin.first-page-info.update')->fill($info) !!}
        {!! Form::text('address', 'Adresa') !!}
        {!! Form::text('phone', 'Telefon') !!}
        {!! Form::text('email', 'Email') !!}
        {!! Form::text('fax', 'Fax') !!}
        {!! Form::text('program', 'Program') !!}
        {!! Form::submit('Modifica') !!}
    {!! Form::close() !!}
@endsection