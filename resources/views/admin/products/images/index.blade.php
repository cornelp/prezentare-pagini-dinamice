@include ('admin.products.images.add')

<div class="d-none">
    {!! Form::open()->delete()->route('admin.products.images.destroy', [$product->id, 0]) !!}
    {!! Form::close() !!}
</div>

@if ($product->images->count())
    @box
        <div class="col-12">
            @foreach ($product->images as $image)
                <div class="card">
                    <img class="card-img-top img-thumbnail" src="{{ asset('images/products/' . $image->name) }}" alt="{{ $image->alt }}">
                    <div class="card-body">
                        <p class="card-text">{{ $image->alt }}</p>
                        <a href="#" onclick="deletePicture(event, {{ $image->id }})">Sterge</a>
                    </div>
                </div>
            @endforeach
        </div>
    @endbox
@endif

@push('js')
    <script>
        let deletePicture = function (event, id) {
            let form = $('.d-none form');

            form.attr('action', form.attr('action').replace(0, id));
            form.submit();

            event.preventDefault();
        };
    </script>
@endpush
