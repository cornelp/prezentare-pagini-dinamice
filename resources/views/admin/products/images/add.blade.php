{!! Form::open()->post()->multipart()->route('admin.products.images.store', [$product->id]) !!}
    {!! Form::file('image', 'Poza noua') !!}
    {!! Form::text('alt', 'Descriere') !!}
    {!! Form::submit('Salveaza poza') !!}
{!! Form::close() !!}
