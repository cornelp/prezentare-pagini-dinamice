@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            <div class="col-md-6">
                @add(['route' => 'admin.products.create', 'name' => 'Produs'])
                @endadd
            </div>

            <div class="col-md-6">
                @search(['route' => 'admin.products.index'])
                @endsearch
            </div>
        @endslot

        @if ($products->count())
            <table class="table table-bordered table-hover">
                @theader(['columns' => [
                    'id' => 'Id',
                    'title' => 'Titlu',
                    'slug' => 'Link',
                    'created_at' => 'Creat la',
                    'updated_at' => 'Modificat la',
                    'none' => 'Actiuni'
                ]])
                @endtheader

                <tbody>
                    @foreach ($products as $product)
                        <tr>
                            <td>{{ $product->id }}</td>
                            <td>{{ $product->titles->first()->pivot->title }}</td>
                            <td>{{ $product->slug }}</td>
                            <td>{{ $product->created_at->format('d.m.Y H:i:s') }}</td>
                            <td>{{ $product->updated_at->format('d.m.Y H:i:s') }}</td>
                            <td>
                                <a href="{{ route('admin.products.edit', $product->id) }}" class="btn btn-xs">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            {{ $products->links() }}
        @else
            Nu exista inregistrari
        @endif
    @endbox
@endsection
