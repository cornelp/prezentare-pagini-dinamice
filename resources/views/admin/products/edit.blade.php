@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            Editeaza produs - "{{ $product->getTranslation()->title }}"
        @endslot

        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab" href="#images">Imagini</a></li>
            @foreach ($langs as $lang)
                <li><a data-toggle="tab" href="#{{ $lang->name }}">{{ $lang->name }}</a></li>
            @endforeach
        </ul>

        <div class="container">
            <div class="tab-content">
                <div id="images" class="tab-pane fade in active">
                    <div>
                        @include('admin.products.images.index')
                    </div>
                </div>
                @foreach ($langs as $lang)
                    <div id="{{ $lang->name }}" class="tab-pane fade">
                        <div>
                            @include('admin.products.lang.edit')
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    @endbox
@endsection

@include('layouts.summernote')
