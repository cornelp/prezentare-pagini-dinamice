{!! Form::open()->put()->multipart()->route('admin.products.lang.update', [$product->id, $lang->id]) !!}

    @php
        $translation = $product->getTranslation($lang->id);
    @endphp

    {!! Form::hidden('lang', $lang->id) !!}
    {!! Form::text('title', 'Titlu', ($translation->fallback ? '' : $translation->title)) !!}
    {!! Form::textarea('description', 'Continut', ($translation->fallback ? '' : $translation->description))->attrs(['class' => 'summernote']) !!}
    {!! Form::file('pdf', 'Fisier PDF (va inlocui un pdf existent pentru limba - daca exista)') !!}
    {!! Form::submit('Salveaza') !!}
{!! Form::close() !!}
