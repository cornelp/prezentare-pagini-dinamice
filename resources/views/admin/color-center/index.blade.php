@extends('adminlte::page')

@section('content')
    {!! Form::open()->post()->route('admin.color-center.store')->multipart() !!}
        {!! Form::file('file', 'PDF') !!}
        {!! Form::text('name', 'Nume') !!}
        {!! Form::submit('Salveaza fisier') !!}
    {!! Form::close() !!}

    @foreach ($items as $item)
        <p>
            <a href="/color-center/{{ $item }}">
                {{ $item }}
            </a>
        </p>
    @endforeach
@endsection
