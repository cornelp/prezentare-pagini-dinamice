@extends('adminlte::page')

@section('content')
    {!! Form::open()->post()->route('admin.instalation-instructions.store')->multipart() !!}
        {!! Form::file('file', 'PDF') !!}
        {!! Form::text('name', 'Nume') !!}
        {!! Form::submit('Salveaza fisier') !!}
    {!! Form::close() !!}

    @foreach ($items as $item)
        <p>
            <a href="/instalation-instructions/{{ $item }}">
                {{ $item }}
            </a>
        </p>
    @endforeach
@endsection
