@extends('adminlte::page')

@section('content')
    <h1>Traducere {{ $lang->name }}:</h1>

    {!! Form::open()->put()->route('admin.frontend.update', [$lang->id]) !!}
        @foreach ($lines as $name => $line)
            {!! Form::text($name, $name, $line) !!}
        @endforeach

        {!! Form::submit('Salveaza') !!}
    {!! Form::close() !!}
@endsection
