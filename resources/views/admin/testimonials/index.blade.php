@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            <div class="col-md-6">
                @add(['route' => 'admin.testimonials.create', 'name' => 'Testimoniale'])
                @endadd
            </div>

            <div class="col-md-6">
                @search(['route' => 'admin.testimonials.index'])
                @endsearch
            </div>
        @endslot

        @if ($items->count())
            <table class="table table-bordered table-hover">
                @theader(['columns' => [
                    'id' => 'Id',
                    'title' => 'Pozitie',
                    'name' => 'Nume',
                    'lang_id' => 'Limba',
                    'active' => 'E activ',
                    'created_at' => 'Creat la',
                    'updated_at' => 'Modificat la',
                    'none' => 'Actiuni'
                ]])
                @endtheader

                <tbody>
                    @foreach ($items as $item)
                        <tr>
                            <td>{{ $item->id }}</td>
                            <td>{{ $item->title }}</td>
                            <td>{{ $item->name }}</td>
                            <td>{{ $item->lang ? $item->lang->name : '' }}</td>
                            <td>{{ $item->active ? 'Da' : 'Nu' }}</td>
                            <td>{{ $item->created_at->format('d.m.Y H:i:s') }}</td>
                            <td>{{ $item->updated_at->format('d.m.Y H:i:s') }}</td>
                            <td>
                                <a href="{{ route('admin.testimonials.edit', $item->id) }}" class="btn btn-xs">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            {{ $items->links() }}
        @else
            Nu exista inregistrari
        @endif
    @endbox
@endsection