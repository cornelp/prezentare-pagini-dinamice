@extends('adminlte::page')

@section('content')

    {!! Form::open()->put()->route('admin.testimonials.update', [$testimonial->id])->fill($testimonial) !!}
        {!! Form::text('title', 'Pozitie') !!}
        {!! Form::text('name', 'Nume') !!}
        {!! Form::select('lang_id', 'Limba', $langs) !!}
        {!! Form::select('active', 'E activa', ['Nu', 'Da']) !!}
        {!! Form::textarea('text', 'Continut') !!}
        {!! Form::submit('Modifica') !!}
    {!! Form::close() !!}
@endsection

@include('layouts.summernote')
