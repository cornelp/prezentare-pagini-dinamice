{!! Form::open()->multipart()->post()->route('admin.first-page.images.store') !!}
    {!! Form::file('image', 'Imagine') !!}
    {!! Form::submit('Adauga') !!}
{!! Form::close() !!}

@foreach ($images as $image)
    <img class="card-img-top img-thumbnail" src="{{ url('/') }}/images/first-page/{{ $image }}">
@endforeach
