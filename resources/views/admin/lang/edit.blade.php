@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            Editeaza limba #{{ $lang->id }}
        @endslot

        {!! Form::open()->put()->route('admin.lang.update', [$lang->id])->fill($lang) !!}
            {!! Form::text('name', 'Nume') !!}
            {!! Form::select('active', 'Activ', ['Nu', 'Da']) !!}
            {!! Form::submit('Modifica') !!}
            {!! Form::anchor('Inapoi')->route('admin.lang.index') !!}

        {!! Form::close() !!}
    @endbox
@endsection
