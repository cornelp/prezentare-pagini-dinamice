@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            <a href="{{ route('admin.lang.index') }}" class="btn btn-default">
                <i class="fa fa-backward"></i>
                Inapoi
            </a>
        @endslot

        {!! Form::open()->route('admin.lang.store') !!}
            {!! Form::text('name', 'Nume') !!}
            {!! Form::select('active', 'Activ', ['Nu', 'Da'], 1) !!}
            {!! Form::submit('Salveaza')->success() !!}
        {!! Form::close() !!}
    @endbox
@endsection
