@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            <div class="col-md-6">
                @add(['route' => 'admin.lang.create', 'name' => 'limba'])
                @endadd
            </div>

            <div class="col-md-6">
                @search(['route' => 'admin.lang.index'])
                @endsearch
            </div>
        @endslot

        @if ($langs->count())
            <table class="table table-bordered table-hover">
                @theader(['columns' => [
                    'id' => 'Id',
                    'name' => 'Nume',
                    'active' => 'Activ',
                    'created_at' => 'Creat la',
                    'updated_at' => 'Modificat la',
                    'none' => 'Actiuni'
                ]])
                @endtheader

                <tbody>
                    @foreach ($langs as $lang)
                        <tr>
                            <td>{{ $lang->id }}</td>
                            <td>{{ $lang->name }}</td>
                            <td>{{ $lang->active ? 'Da' : 'Nu' }}</td>
                            <td>{{ $lang->created_at->format('d.m.Y H:i:s') }}</td>
                            <td>{{ $lang->updated_at->format('d.m.Y H:i:s') }}</td>
                            <td>
                                <a href="{{ route('admin.lang.edit', $lang->id) }}" class="btn btn-xs">
                                    <i class="fa fa-edit"></i>
                                </a>
                                <a href="{{ route('admin.frontend.edit', $lang->id) }}" class="btn btn-xs">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            {{ $langs->links() }}
        @else
            Nu exista inregistrari
        @endif
    @endbox
@endsection
