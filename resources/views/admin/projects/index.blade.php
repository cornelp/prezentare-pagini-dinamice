@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            <div class="col-md-6">
                @add(['route' => 'admin.projects.create', 'name' => 'Proiect'])
                @endadd
            </div>

            <div class="col-md-6">
                @search(['route' => 'admin.projects.index'])
                @endsearch
            </div>
        @endslot

        @if ($projects->count())
            <table class="table table-bordered table-hover">
                @theader(['columns' => [
                    'id' => 'Id',
                    'title' => 'Titlu',
                    'slug' => 'Link',
                    'created_at' => 'Creat la',
                    'updated_at' => 'Modificat la',
                    'none' => 'Actiuni'
                ]])
                @endtheader

                <tbody>
                    @foreach ($projects as $project)
                        <tr>
                            <td>{{ $project->id }}</td>
                            <td>{{ $project->titles->first()->pivot->title }}</td>
                            <td>{{ $project->slug }}</td>
                            <td>{{ $project->created_at->format('d.m.Y H:i:s') }}</td>
                            <td>{{ $project->updated_at->format('d.m.Y H:i:s') }}</td>
                            <td>
                                <a href="{{ route('admin.projects.edit', $project->id) }}" class="btn btn-xs">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            {{ $projects->links() }}
        @else
            Nu exista inregistrari
        @endif
    @endbox
@endsection
