{!! Form::open()->put()->route('admin.projects.lang.update', [$project->id, $lang->id]) !!}

    @php
        $translation = $project->getTranslation($lang->id);
    @endphp

    {!! Form::hidden('lang', $lang->id) !!}
    {!! Form::text('title', 'Titlu', ($translation->fallback ? '' : $translation->title)) !!}
    {!! Form::textarea('description', 'Continut', ($translation->fallback ? '' : $translation->description))->attrs(['class' => 'summernote']) !!}
    {!! Form::submit('Salveaza') !!}
{!! Form::close() !!}
