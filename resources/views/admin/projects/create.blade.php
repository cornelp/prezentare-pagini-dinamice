@extends('adminlte::page')

@section('content')
    @box
        @slot('buttons')
            <a href="{{ route('admin.projects.index') }}" class="btn btn-default">
                <i class="fa fa-backward"></i>
                Inapoi
            </a>
            <p>(Limba {{ $lang->name }})</p>
        @endslot

        {!! Form::open()->route('admin.projects.store') !!}
            {!! Form::text('slug', 'Titlu') !!}
            {!! Form::textarea('description', 'Continut')->attrs(['class' => 'summernote']) !!}
            {!! Form::submit('Salveaza')->success() !!}
        {!! Form::close() !!}
    @endbox
@endsection

@push('css')
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.css">
@endpush

@push('js')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/summernote.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.11/lang/summernote-ro-RO.min.js"></script>

    <script>
        $(function () {
            $('.summernote').summernote();
        });
    </script>
@endpush
