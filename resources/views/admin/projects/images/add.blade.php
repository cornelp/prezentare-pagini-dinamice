{!! Form::open()->multipart()->route('admin.projects.images.store', [$project->id, 0]) !!}
    {!! Form::file('image', 'Poza noua') !!}
    {!! Form::text('alt', 'Descriere') !!}
    {!! Form::submit('Salveaza poza') !!}
{!! Form::close() !!}
