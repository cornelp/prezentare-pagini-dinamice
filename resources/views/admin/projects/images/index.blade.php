@include ('admin.projects.images.add')

<div class="d-none">
    {!! Form::open()->delete()->route('admin.projects.images.destroy', [$project->id, 0]) !!}
    {!! Form::close() !!}
</div>

@if ($project->images->count())
    @box
        <div class="col-12">
            @foreach ($project->images as $image)
                <div class="card">
                    <img class="card-img-top img-thumbnail" src="{{ asset('images/' . $image->name) }}" alt="{{ $image->alt }}">
                    <div class="card-body">
                        <p class="card-text">{{ $image->alt }}</p>
                        <a href="#" onclick="deletePicture(event, {{ $image->id }})">Sterge</a>
                    </div>
                </div>
            @endforeach
        </div>
    @endbox
@endif

@push('js')
    <script>
        let deletePicture = function (event, id) {
            let form = $('.d-none form');

            form.attr('action', form.attr('action').replace(0, id));
            form.submit();

            event.preventDefault();
        };
    </script>
@endpush
