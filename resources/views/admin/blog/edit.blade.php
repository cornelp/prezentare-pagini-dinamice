@extends('adminlte::page')

@section('content')

    {!! Form::open()->put()->route('admin.blogs.update', [$blog->id])->fill($blog) !!}
        {!! Form::text('title', 'Titlu') !!}
        {!! Form::select('lang_id', 'Limba', $langs) !!}
        {!! Form::textarea('text', 'Continut') !!}
        {!! Form::submit('Modifica') !!}
    {!! Form::close() !!}
@endsection

@include('layouts.summernote')
