@extends('layouts.master')

@section('content')

    @include('layouts.slider')

<div class="section call-for-action">
    <div class="container">
        <div class="row">
            <div class="col-sm-9">
                <div class="custom_heading home_quote_txt">
                    <div class="call-for-action-text">
                        @lang('frontend.text_cotatie')
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                <div class="home_quote_btn text-right">
                    <a href="/contact">@lang('frontend.buton_cotatie')</a>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section home-latest-projects">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="mb-3 projects_btn">
                    <p>@lang('frontend.ultimile_proiecte')</p>
                    <div class="navigationbutton button-left projects_btn_left">
                        <i class="fa fa-angle-left"></i>
                    </div>
                    <div class="navigationbutton button-right projects_btn_right">
                        <i class="fa fa-angle-right"></i>
                    </div>
                </div>
                <div class="myportfolio-container minimal-light">
                    <div class="slider-wrapper">
                        <div class="row">
                            <ul class="project-list">
                                @foreach ($projects as $project)
                                    @php
                                        $image = $project->images->first();
                                    @endphp

                                    <li class="our-projects-wrapper col-md-4 col-sm-6">
                                        <div class="project-media-cover-wrapper">
                                            <div class="project-entry-media">
                                                <img src="images/{{ $image->name ?? '' }}" alt="{{ $image->alt ?? '' }}" />
                                            </div>
                                            <div class="project-entry-cover">
                                                <div class="project-overlay our-projects-container"></div>
                                                <div class="project-content">
                                                    <div class="our-projects-categories">
                                                        <a href="/projects/{{ $project->id }}">Projects</a>
                                                    </div>
                                                    <div class="line-clear"></div>
                                                    <div class="our-projects-title">
                                                        <a href="/projects/{{ $project->id }}">
                                                            {{ $project->getTranslation()->title }}
                                                        </a>
                                                    </div>
                                                    <div class="line-clear"></div>
                                                    <div class="our-projects-popup">
                                                        <a data-rel="prettyPhoto" href="images/{{ $image->name ?? '' }}">
                                                            <i class="fa fa-search"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="section pt-9 pb-9 parallax bg-1">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="parallax-text">
                    @lang('frontend.text_homepage')
                </div>
            </div>
        </div>
    </div>
</div>
@if (isset($testimonials) && count($testimonials))
<div class="section pt-5 pb-5 bg-gray testimonials">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="section-title">@lang('frontend.testimoniale')</div>
                <div class="testimonials_wrapper row">
                    <div class="testimonials-list">
                        @foreach ($testimonials as $item)
                        <div class="quote col-md-6 col-sm-6">
                            <blockquote class="testimonials-text">
                                <p>{!! $item->text !!}</p>
                            </blockquote>
                            <img width="90" height="90" src="images/testimonials/{{ $item->image }}" class="avatar" alt="" />
                            <cite class="author">
                                <span>{{ $item->name }}</span>
                                <span class="title">{{ $item->title }}</span>
                            </cite>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
<div class="section pt-5 pb-5 bg-gray testimonials">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="section-title">@lang('frontend.parteneri')</div>
                <div class="row">
					<div class="col-md-3 col-sm-3 col-xs-3">
						<img src="images/partener1.jpg" class="avatar" alt="" />
					</div>
					<div class="col-md-3 col-sm-3 col-xs-3">
						<img src="images/partener2.jpg" class="avatar" alt="" />
					</div>
					<div class="col-md-3 col-sm-3 col-xs-3">
						<img src="images/partener3.jpg" class="avatar" alt="" />
					</div>
					<div class="col-md-3 col-sm-3 col-xs-3">
						<img src="images/partener4.png" class="avatar" alt="" />
					</div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
