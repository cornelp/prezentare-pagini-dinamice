@extends('layouts.master')

@section('content')
    <div class="container">
        @if (view()->exists('terms.' . lang()))
            @include('terms.' . lang())
        @endif
    </div>
@endsection
