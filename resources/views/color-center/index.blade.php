@extends('layouts.master')

    @section('content')
        @if (count($items))
            @foreach ($items as $item)
                <p>
                    <a href="/color-center/{{ $item }}">
                        {{ $item }}
                    </a>
                </p>
            @endforeach
        @endif
@endsection
