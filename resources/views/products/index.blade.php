@extends('layouts.master')

@section('content')
<div class="big-title">
    <div class="container">
        <h1 class="entry-title">News Grid</h1>
        <div class="breadcrumb">
            <div class="container">
                <ul class="tm_bread_crumb">
                    <li class="top"><a href="/">Home</a></li>
                    <li class="sub tail current">Products</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="main">
    <div class="container">
        <div class="row">
            <div class="news-content">
                <ul class="news-grid mb-3">
                    @foreach ($products as $product)
                        <li class="col-md-4 col-sm-6 mb-3">
                            <div class="news-entry-media">
                                @php
                                    $image = optional($product->images->first());
                                @endphp

                                <img src="/images/products/{{ $image->name ?? '' }}" alt="{{ $image->alt ?? '' }}" />
                                <div class="news-entry-cover">
                                    <div class="news-overlay home-blog-container"></div>
                                    <div class="home-blog-popup">
                                        <a data-rel="prettyPhoto" href="images/products/{{ $image->name ?? '' }}">
                                            <i class="fa fa-search"></i>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="home-blog-content">
                                <div class="home-blog-title">
                                    <a href="/products/{{ $product->id }}">
                                        {{ $product->getTranslation()->title }}
                                    </a>
                                </div>
                                <div class="home-blog-date">{{ $product->created_at->format('d.m.Y') }}</div>
                                <div class="home-blog-desc">
                                    {!! substr($product->getTranslation()->description, 0, 85) . '...' !!}
                                </div>
                                <div class="home-blog-readmore">
                                    <a href="/products/{{ $product->id }}">
                                        Read More
                                    </a>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>

                {{ $products->links() }}
            </div>
        </div>
    </div>
</div>

@endsection
