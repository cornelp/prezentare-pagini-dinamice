@extends('layouts.master')

@section('content')
<div class="big-title">
    <div class="container">
        <h1 class="entry-title">{{ $product->getTranslation()->title }}</h1>
        <div class="breadcrumb">
            <div class="container">
                <ul class="tm_bread_crumb">
                    <li class="top"><a href="/">Home</a></li>
                    <li class="sub"><a href="/projects">Product</a></li>
                    <li class="sub tail current">Product Detail</li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="main">
    <div class="single-project">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="gallery tm-gallery mb-3">
                        <div class="tm-nav">
                            <span class="tm-next"><i class="fa fa-angle-right"></i></span>
                            <span class="tm-prev"><i class="fa fa-angle-left"></i></span>
                        </div>
                        <div class="project-slider">
                            @foreach ($product->images as $image)
                                <img width="1900" height="1268" src="/images/products/{{ $image->name }}" alt="{{ $image->alt }}" />
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="single-project-description">
                        <h3 class="heading-title">{{ $product->getTranslation()->title }}</h3>
                        <p>
                            {!! $product->getTranslation()->description !!}
                        </p>
                    </div>
                </div>
                <div class="col-md-4">
					
                    @if (file_exists(public_path('/pdf/' . $product->slug . '-' . $product->getCurrentLang()->short_name . '.pdf')))
                        <a href="/pdf/{{ $product->slug . '-' . $product->getCurrentLang()->short_name . '.pdf' }}">Download PDF</a>
                    @endif

                </div>
            </div>
        </div>
    </div>
</div>
@endsection
