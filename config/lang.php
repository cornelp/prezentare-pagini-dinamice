<?php

return [
    'default' => env('DEFAULT_LANGUAGE', 1),
    'filename' => env('LANG_FILE_NAME', 'frontend')
];
