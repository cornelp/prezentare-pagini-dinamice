<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::prefix('admin')
    ->namespace('Admin')
    ->name('admin.')
    ->group(function () {
        Route::resource('lang', 'LangController')->except('show');
        Route::resource('projects', 'ProjectController')->except('show');
        Route::resource('projects.images', 'ProjectImageController')->except('show');
        Route::resource('projects.lang', 'ProjectLangController')->except('show');

        Route::resource('products', 'ProductController')->except('show');
        Route::resource('products.images', 'ProductImageController')->except('show');
        Route::resource('products.lang', 'ProductLangController')->except('show');

        Route::get('about', 'AboutController@edit')->name('about.edit');
        Route::put('about', 'AboutController@update')->name('about.update');

        Route::get('frontend/{lang}', 'FrontendController@edit')->name('frontend.edit');
        Route::put('frontend/{lang}', 'FrontendController@update')->name('frontend.update');

        Route::get('/', 'DashboardController@index');

        Route::get('first-page', 'FirstPageController@index')->name('first-page.index');
        Route::post('first-page', 'FirstPageController@saveImage')->name('first-page.images.store');

        Route::get('first-page-info', 'FirstPageInfoController@edit')->name('first-page-info.show');
        Route::post('first-page-info', 'FirstPageInfoController@update')->name('first-page-info.update');

        Route::get('material-estimator', 'MaterialEstimatorController@index')->name('material-estimator.index');
        Route::put('material-estimator', 'MaterialEstimatorController@update')->name('material-estimator.update');

        Route::get('color-center', 'ColorCenterController@index')->name('color-center.index');
        Route::post('color-center', 'ColorCenterController@store')->name('color-center.store');

        Route::get('instalation-instructions', 'InstalationInstructionController@index')->name('instalation-instructions.index');
        Route::post('instalation-instructions', 'InstalationInstructionController@store')->name('instalation-instructions.store');

        Route::get('terms', 'TermsController@edit')->name('terms.edit');
        Route::put('terms', 'TermsController@update')->name('terms.update');

        Route::get('confident', 'ConfidentController@edit')->name('confident.edit');
        Route::put('confident', 'ConfidentController@update')->name('confident.update');

        Route::resource('blog', 'BlogController');

        Route::resource('testimonials', 'TestimonialController');
        Route::resource('blogs', 'BlogController');
    });

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/consent', 'HomeController@setConsent');
Route::get('/about-us', 'HomeController@about');
Route::get('/projects', 'ProjectController@index');
Route::get('/projects/{project}', 'ProjectController@show');
Route::get('/products', 'ProductController@index');
Route::get('/products/{product}', 'ProductController@show');
Route::put('/lang', 'HomeController@lang');

Route::get('contact', 'HomeController@showContact');
Route::post('contact', 'HomeController@sendContact');

Route::get('material-estimator', 'HomeController@showMaterialEstimator');
Route::get('instalation-instructions', 'HomeController@showInstalationInstructions');
Route::get('color-center', 'HomeController@showColorCenter');

Route::get('terms', 'HomeController@showTerms');
Route::get('confident', 'HomeController@showConfident');

Route::get('/blog', 'HomeController@showBlogs');
Route::get('/blog/{slug}', 'HomeController@showBlog');
