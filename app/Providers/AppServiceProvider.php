<?php

namespace App\Providers;

use Illuminate\Support\Facades\Blade;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Project;
use App\Lang;
use App\Product;
use App\FirstPageInfo;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Blade::component('components.box', 'box');
        Blade::component('components.search', 'search');
        Blade::component('components.add', 'add');
        Blade::component('components.theader', 'theader');
        Blade::component('components.validation', 'validation');

        Blade::directive('float', function ($value) {
            return "<?php echo number_format({$value}, 2); ?>";
        });

        // app()->setLocale('en');

        if (!$this->app->runningInConsole() && !request()->is('admin/*')) {
            view()->share('firstPageInfo', FirstPageInfo::first());
            view()->share('projects', Project::all());
            view()->share('products', Product::all());
            view()->share('langs', Lang::actives()->get());
        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
