<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = ['name', 'alt'];

    public function projects()
    {
        return $this->morphedByMany(Project::class, 'imageable');
    }

    public function products()
    {
        return $this->morphedByMany(Product::class, 'imageable');
    }
}
