<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Testimonal extends Model
{
    protected $guarded = ['id'];
    protected $casts = ['active' => 'boolean'];

    public function lang()
    {
        return $this->belongsTo(Lang::class);
    }
}
