<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FirstPageInfo extends Model
{
    protected $table = 'first_page_info';

    protected $guarded = ['id'];
}
