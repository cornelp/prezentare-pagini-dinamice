<?php

namespace App;

use File;

class Project extends Model
{
    protected $fillable = ['slug', 'is_active'];
    protected $casts = ['is_active' => 'boolean'];
    protected $with = ['titles'];

    public static function boot()
    {
        parent::boot();

        static::created(function ($model) {
            $lang = Lang::find(config('lang.default'));

            $model->titles()->attach($lang, ['title' => $model->title]);
        });
    }

    public function getTitleAttribute()
    {
        $array = array_map(function ($item) {
            return ucfirst($item);
        }, explode('-', $this->slug));

        return implode(' ', $array);
    }

    public function setSlugAttribute($value)
    {
        $this->attributes['slug'] = trim(strtolower(str_replace(' ', '-', $value)));
    }

    public function images()
    {
        return $this->morphToMany(Image::class, 'imageable');
    }

    public function titles()
    {
        return $this->belongsToMany(Lang::class)
            ->withPivot('title');
    }

    public function saveDescription($description, $forLang = null)
    {
        $forLang = $forLang ?? config('lang.default');
        $path = resource_path('views/projects/' . $this->slug);
        $view = $path . '/' . $forLang . '.blade.php';

        if (! File::exists($path)) {
            File::makeDirectory($path, 0777, true);
        }

        File::put($view, $description);
    }

    public function getTranslation($lang = null)
    {
        $lang = $lang ?? lang();

        $result = new \stdClass;
        $result->fallback = false;

        $title = $this->titles->where('id', $lang)->first();

        if (is_null($title)) {
            $title = $this->titles->first();

            $lang = config('lang.default');

            $result->fallback = true;
        }

        $result->name = $title->name;
        $result->title = $title->pivot->title;
        $result->description = view('projects.' . $this->slug . '.' . $lang)->render();

        return $result;
    }
}
