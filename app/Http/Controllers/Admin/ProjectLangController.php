<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Lang;
use App\Project;
use App\Http\Controllers\Controller;

class ProjectLangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Project $project, Lang $lang)
    {
        $this->validate(request(), [
            'title' => 'required|min:1|max:150',
            'description' => 'required|min:1'
        ]);

        // find specified language
        $existing = $project->titles->search(function ($item) use ($lang) {
            return $item->id == $lang->id;
        });

        // save / update title for specified language
        $existing !== false
            ? $project->titles()->updateExistingPivot($lang->id, ['title' => request('title')])
            : $project->titles()->attach($lang->id, ['title' => request('title')]);

        // save description
        $project->saveDescription(request('description'), $lang->id);

        return redirect()->route('admin.projects.edit', [$project->id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
