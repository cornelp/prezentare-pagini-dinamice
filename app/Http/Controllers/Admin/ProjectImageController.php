<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Project;
use App\Http\Controllers\Controller;
use App\Image;

class ProjectImageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Project $project)
    {
        $attrs = $this->validate($request, [
            'image' => 'required|file',
            'alt' => 'required'
        ]);

        $request->image->move(
            public_path('images'),
            $filename = now()->format('Ymdhis') . '.' . $request->image->getClientOriginalExtension()
        );

        // save reference to db
        $project->images()->save(Image::create([
            'name' => $filename,
            'alt' => request('alt')
        ]));

        // $project->images()->create([
        //     'name' => $filename,
        //     'alt' => $request->alt
        // ]);

        return redirect()->route('admin.projects.edit', $project->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
