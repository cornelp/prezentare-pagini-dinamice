<?php

namespace App\Http\Controllers\Admin;

use App\Lang;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class LangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $langs = Lang::paginate(10);

        return view('admin.lang.index', compact('langs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.lang.create');
    }

    protected function validateFields($request)
    {
        return $this->validate($request, [
            'name' => 'required',
            'active' => 'boolean'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Lang::create($this->validateFields($request));

        return redirect()->route('admin.lang.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Lang  $lang
     * @return \Illuminate\Http\Response
     */
    public function show(Lang $lang)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Lang  $lang
     * @return \Illuminate\Http\Response
     */
    public function edit(Lang $lang)
    {
        return view('admin.lang.edit', compact('lang'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Lang  $lang
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Lang $lang)
    {
        $lang->update($this->validateFields($request));

        return redirect()->route('admin.lang.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Lang  $lang
     * @return \Illuminate\Http\Response
     */
    public function destroy(Lang $lang)
    {
        //
    }
}
