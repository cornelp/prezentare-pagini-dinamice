<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Lang;

class ConfidentController extends Controller
{
    public function edit()
    {
        $langs = Lang::actives()->get();
        $contents = [];

        foreach ($langs as $lang) {
            $view = 'confident.' . $lang->id;

            $contents[$lang->id] = view()->exists($view)
                ? view($view)->render() : '';
        }

        return view('admin.confident.index', compact('contents', 'langs'));
    }

    public function update()
    {
        $this->validate(request(), [
            'content' => 'required|min:1',
            'lang_id' => 'required'
        ]);

        \File::put(resource_path('views/confident/' . request('lang_id', 1) . '.blade.php'), request('content'));

        return redirect()->route('admin.confident.edit');
    }
}
