<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class InstalationInstructionController extends Controller
{
    public function __construct()
    {
        if (!\File::exists(public_path('instalation-instructions'))) {
            \File::makeDirectory(public_path('instalation-instructions'));
        }
    }

    public function index()
    {
        $items = array_diff(scandir(public_path('instalation-instructions/')), ['..', '.']);

        return view('admin.instalation-instructions.index', compact('items'));
    }

    public function store()
    {
        $this->validate(request(), [
            'file' => 'required|file|mimes:pdf',
            'name' => 'required'
        ]);

        request()->file->move(
            public_path('instalation-instructions'),
            request('name') . '.' . request()->file->getClientOriginalExtension()
        );

        return redirect()->back();
    }
}
