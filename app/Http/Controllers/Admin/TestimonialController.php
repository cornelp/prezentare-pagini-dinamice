<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Testimonal;
use App\Lang;

class TestimonialController extends Controller
{
    public function index()
    {
        $items = Testimonal::with('lang')
            ->paginate(10);

        return view('admin.testimonials.index', compact('items'));
    }

    public function create()
    {
        $langs = Lang::whereActive(true)->pluck('name', 'id')->toArray();

        return view('admin.testimonials.create', compact('langs'));
    }

    public function store()
    {
        $path = public_path('images/testimonials');

        if (!\File::exists($path)) {
            \File::makeDirectory($path);
        }

        $attrs = $this->validate(request(), [
            'name' => 'required',
            'title' => 'required',
            'text' => 'required',
            'image' => 'file|nullable',
            'lang_id' => 'numeric'
        ]);

        if (request()->has('image') && null !== request()->image) {
            request()->image->move(
                $path,
                $attrs['image'] = now()->format('YmdHi') . '.' . request()->image->getClientOriginalExtension()
            );
        }

        Testimonal::create($attrs);

        return redirect()->route('admin.testimonials.index');
    }

    public function edit(Testimonal $testimonial)
    {
        $langs = Lang::whereActive(true)->pluck('name', 'id')->toArray();

        return view('admin.testimonials.edit', compact('testimonial', 'langs'));
    }

    public function update(Testimonal $testimonial)
    {
        $this->validate(request(), [
            'title' => 'required',
            'name' => 'required',
            'text' => 'required',
            'active' => 'required|boolean',
        ]);

        $testimonial->update(request(['title', 'name', 'text', 'active']));

        return redirect()->route('admin.testimonials.index');
    }
}
