<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Lang;

class TermsController extends Controller
{
    public function edit()
    {
        $langs = Lang::actives()->get();
        $contents = [];

        foreach ($langs as $lang) {
            $view = 'terms.' . $lang->id;

            $contents[$lang->id] = view()->exists($view)
                ? view($view)->render() : '';
        }

        return view('admin.terms.index', compact('contents', 'langs'));
    }

    public function update()
    {
        $this->validate(request(), [
            'content' => 'required|min:1',
            'lang_id' => 'required'
        ]);

        \File::put(resource_path('views/terms/' . request('lang_id', 1) . '.blade.php'), request('content'));

        return redirect()->route('admin.terms.edit');
    }
}
