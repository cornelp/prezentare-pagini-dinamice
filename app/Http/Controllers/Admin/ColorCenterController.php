<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class ColorCenterController extends Controller
{
    public function index()
    {
        $path = public_path('color-center/');

        if (!\File::exists($path)) {
            \File::makeDirectory($path);
        }

        $items = array_diff(scandir($path), ['..', '.']);

        return view('admin.color-center.index', compact('items'));
    }

    public function store()
    {
        $this->validate(request(), [
            'file' => 'required|file|mimes:pdf',
            'name' => 'required'
        ]);

        request()->file->move(
            public_path('color-center'),
            request('name') . '.' . request()->file->getClientOriginalExtension()
        );

        return redirect()->back();
    }
}
