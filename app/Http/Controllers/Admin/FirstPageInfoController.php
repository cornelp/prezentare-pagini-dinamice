<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\FirstPageInfo;

class FirstPageInfoController extends Controller
{
    public function edit()
    {
        $info = FirstPageInfo::first();

        return view('admin.first-page-info.edit', compact('info'));
    }

    public function update()
    {
        $attrs = request(['address', 'phone', 'email', 'fax', 'program']);
        $firstPageInfo = FirstPageInfo::first();

        $firstPageInfo
            ? $firstPageInfo->update($attrs)
            : FirstPageInfo::create($attrs);

        return redirect()->back();
    }
}
