<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Lang;
use App\Blog;

class BlogController extends Controller
{
    public function index()
    {
        $items = Blog::with('lang')
            ->paginate(10);

        return view('admin.blog.index', compact('items'));
    }

    public function create()
    {
        $langs = Lang::whereActive(true)->pluck('name', 'id')->toArray();

        return view('admin.blog.create', compact('langs'));
    }

    public function store()
    {
        $path = public_path('images/blog');

        if (!\File::exists($path)) {
            \File::makeDirectory($path);
        }

        $attrs = $this->validate(request(), [
            'title' => 'required',
            'text' => 'required',
            'image' => 'file|nullable',
            'lang_id' => 'numeric'
        ]);

        if (request()->has('image') && null !== request()->image) {
            request()->image->move(
                $path,
                $attrs['image'] = now()->format('YmdHi') . '.' . request()->image->getClientOriginalExtension()
            );
        }

        Blog::create($attrs);

        return redirect()->route('admin.blog.index');
    }

    public function edit(Blog $blog)
    {
        $langs = Lang::whereActive(true)->pluck('name', 'id')->toArray();

        return view('admin.blog.edit', compact('blog', 'langs'));
    }

    public function update(Blog $blog)
    {
        $this->validate(request(), [
            'title' => 'required',
            'name' => 'required',
            'text' => 'required',
            'active' => 'required|boolean',
        ]);

        $blog->update(request(['title', 'name', 'text', 'active']));

        return redirect()->route('admin.blog.index');
    }
}
