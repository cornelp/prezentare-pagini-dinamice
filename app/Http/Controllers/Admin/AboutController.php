<?php

namespace App\Http\Controllers\Admin;

use App\Lang;
use App\Http\Controllers\Controller;

class AboutController extends Controller
{
    public function edit()
    {
        $langs = Lang::actives()->get();
        $contents = [];

        foreach ($langs as $lang) {
            $view ='about.' . $lang->id;

            $contents[$lang->id] = view()->exists($view)
                ? view($view)->render() : '';
        }

        return view('admin.about.index', compact('contents', 'langs'));
    }

    public function update()
    {
        $this->validate(request(), [
            'content' => 'required|min:1',
            'lang_id' => 'required'
        ]);

        \File::put(resource_path('views/about/' . request('lang_id', 1) . '.blade.php'), request('content'));

        return redirect()->route('admin.about.edit');
    }
}
