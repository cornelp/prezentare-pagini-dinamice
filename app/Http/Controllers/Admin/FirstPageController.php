<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;

class FirstPageController extends Controller
{
    public function index()
    {
        $images = array_diff(scandir(public_path('images/first-page')), ['..', '.']);

        return view('admin.first-page.index', compact('images'));
    }

    public function saveImage()
    {
        if (!\File::exists(public_path('images/first-page'))) {
            \File::makeDirectory(public_path('images/first-page'));
        }

        $this->validate(request(), [
            'image' => 'required|file'
        ]);

        request()->image->move(
            public_path('images/first-page/'),
            now()->format('YmdHis') . '.' . request()->image->getClientOriginalExtension()
        );

        return redirect()->route('admin.first-page.index');
    }
}
