<?php

namespace App\Http\Controllers\Admin;

use App\Lang;
use App\Http\Controllers\Controller;

class MaterialEstimatorController extends Controller
{
    public function __construct()
    {
        $path = resource_path('views/material-estimator');

        if (!\File::exists($path)) {
            \File::makeDirectory($path);
        }
    }

    public function index()
    {
        $langs = Lang::actives()->get();
        $contents = [];

        foreach ($langs as $lang) {
            $view = 'material-estimator.' . $lang->id;

            $contents[$lang->id] = view()->exists($view)
                ? view($view)->render() : '';
        }

        return view('admin.material-estimator.index', compact('contents', 'langs'));
    }

    public function update()
    {
        $this->validate(request(), [
            'content' => 'required|min:1',
            'lang_id' => 'required'
        ]);

        \File::put(resource_path('views/material-estimator/' . request('lang_id', 1) . '.blade.php'), request('content'));

        return redirect()->route('admin.material-estimator.index');
    }
}
