<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Lang;

class FrontendController extends Controller
{
    public function edit(Lang $lang)
    {
        if ($lang->short_name === '') {
            $lang = Lang::find(lang());
        }

        $lines = include_once resource_path('lang/' . $lang->short_name . '/' . config('lang.filename') . '.php');

        return view('admin.frontend.show', compact('lines', 'lang'));
    }

    public function update(Lang $lang)
    {
        $request = request()->except('_method', '_token');

        \File::put(
            resource_path('lang/' . $lang->short_name . '/' . config('lang.filename') . '.php'),
            '<?php return ' . var_export($request, true) . ';'
        );

        return redirect()->route('admin.frontend.edit', [$lang->id]);
    }
}
