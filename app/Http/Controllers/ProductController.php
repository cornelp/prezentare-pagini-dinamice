<?php

namespace App\Http\Controllers;

use App\Product;


class ProductController extends Controller
{
    public function index()
    {
        $products = Product::with('images')->paginate(10);

        return view('products.index', compact('products'));
    }

    public function show(Product $product)
    {
        $product->load('images');

        return view('products.show', compact('product'));
    }
}
