<?php

namespace App\Http\Controllers;

use App\Project;
use App\Mail\ContactMail;
use App\Lang;
use App\Testimonal;
use App\Blog;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index()
    {
        $path = public_path('images/first-page');

        if (!\File::exists($path)) {
            \File::makeDirectory($path);
        }

        $images = preg_grep('~\.(jpeg|jpg|png)$~', scandir($path));
        $projects = Project::with('images')->take(4)->get();
        $testimonials = Testimonal::whereLangId(lang())->whereActive(true)->get();

        return view('home', compact('images', 'projects', 'testimonials'));
    }

    public function setConsent()
    {
        $consent = request('accept', 0);

        session(compact('consent'));

        return redirect('/');
    }

    public function about()
    {
        return view('about.master');
    }

    public function lang()
    {
        $lang = Lang::findOrFail(request('lang', 1));

        session(['lang' => $lang->id]);
        app()->locale($lang->short_name);

        return redirect()->back();
    }

    public function showContact()
    {
        return view('contact.index');
    }

    public function sendContact()
    {
        $data = $this->validate(request(), [
            'name' => 'required',
            'phone' => 'required|min:10',
            'email' => 'required|email',
            'message' => 'required|min:10'
        ]);

        // return new ContactMail($data);
        \Mail::send(new ContactMail($data));

        return redirect()->back();
    }

    public function showMaterialEstimator()
    {
        return view('material-estimator.master');
    }

    public function showInstalationInstructions()
    {
        $items = array_diff(scandir(public_path('instalation-instructions/')), ['..', '.']);

        return view('instalation-instructions.index', compact('items'));
    }

    public function showTerms()
    {
        return view('terms.master');
    }

    public function showConfident()
    {
        return view('confident.master');
    }

    public function showBlog($slug)
    {
        $slug = explode('-', $slug);
        $id = $slug[count($slug) - 1];

        $blog = Blog::findOrFail($id);
        $others = Blog::take(3)->get();

        return view('blog.show', compact('blog', 'others'));
    }

    public function showBlogs()
    {
        $blogs = Blog::paginate(10);
        $firstPost = $blogs->shift();

        return view('blog.index', compact('blogs', 'firstPost'));
    }

    public function showColorCenter()
    {
        $path = public_path('color-center');

        if (!\File::exists($path)) {
            \File::makeDirectory($path);
        }

        $items = array_diff(scandir($path), ['..', '.']);

        return view('color-center.index', compact('items'));
    }
}
