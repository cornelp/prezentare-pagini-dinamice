<?php

if (! function_exists('lang')) {
    function lang() {
        return session('lang', config('lang.default'));
    }
}
