<?php

namespace App;

class Lang extends Model
{
    protected $fillable = ['name', 'active'];
    protected $casts = ['active' => 'boolean'];

    public function getShortNameAttribute()
    {
        return substr(strtolower($this->name), 0, 2);
    }

    public static function boot()
    {
        parent::boot();

        static::created(function ($model) {
            // create folder if it does not exist
            if (!\File::exists(resource_path("lang/$model->short_name"))) {
                \File::makeDirectory(resource_path("lang/$model->short_name"));
            }

            // copy translation file
            \File::copy(
                resource_path('lang/' . config('app.locale') . '/' . config('lang.filename') . '.php'),
                resource_path('lang/' . $model->short_name . '/' . config('lang.filename') . '.php')
            );
        });
    }

    public function scopeActives($query)
    {
        $query->where('active', true);
    }
}
