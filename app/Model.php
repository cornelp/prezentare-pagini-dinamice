<?php

namespace App;

use Illuminate\Database\Eloquent\Model as BaseModel;

class Model extends BaseModel
{
    public static function boot()
    {
        parent::boot();

        static::addGlobalScope(function ($query) {
            $query->orderBy(request('sort', 'id'), request('order', 'asc'));
        });
    }
}
