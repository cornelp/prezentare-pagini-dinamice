<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    protected $guarded = ['id'];

    public function lang()
    {
        return $this->belongsTo(Lang::class);
    }

    public function getSlugAttribute()
    {
        return str_slug($this->title . ' ' . $this->id);
    }
}
